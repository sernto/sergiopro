/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimiento;

import javax.persistence.EntityManager;
import persistencia.Cliente;
import persistencia.DetalleFacturas;

/**
 *
 * @author jonathan.rodriguez
 */
public class mantenimiento_detalla_factura {

    public int guardarDatos(DetalleFacturas deta) {
        EntityManager em = JpaUtil.getEntityManagerFactory().createEntityManager();
        int flag = 0;
        em.getTransaction().begin();
        try {
            em.persist(deta);
            em.getTransaction().commit();
            flag = 1;
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return flag;
    }
    
    public static void main(String[] args) {
        DetalleFacturas objeto=new DetalleFacturas(0);
        objeto.setId_factura(1);
        objeto.setId_producto(1);
        objeto.setCantidad(4);
        objeto.setMontoFinalProducto(10.0);
        mantenimiento_detalla_factura man=new mantenimiento_detalla_factura();
        int numero=man.guardarDatos(objeto);
        if (numero==1) {
            System.out.println("exito en insertar datos");
        } else {
            System.out.println("error al insertar datos");
        }
    }
}

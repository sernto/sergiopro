/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jonathan.rodriguez
 */
@Entity
@Table(name = "detalle_facturas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleFacturas.findAll", query = "SELECT d FROM DetalleFacturas d")
    , @NamedQuery(name = "DetalleFacturas.findByIdDetalle", query = "SELECT d FROM DetalleFacturas d WHERE d.idDetalle = :idDetalle")
    , @NamedQuery(name = "DetalleFacturas.findByCantidad", query = "SELECT d FROM DetalleFacturas d WHERE d.cantidad = :cantidad")
    , @NamedQuery(name = "DetalleFacturas.findByMontoFinalProducto", query = "SELECT d FROM DetalleFacturas d WHERE d.montoFinalProducto = :montoFinalProducto")})
public class DetalleFacturas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)

    @Column(name = "id_detalle")
    private Integer idDetalle;
    @Column(name = "id_factura")
    private Integer id_factura;
    @Column(name = "id_producto")
    private Integer id_producto;
    @Column(name = "cantidad")
    private Integer cantidad;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "monto_final_producto")
    private Double montoFinalProducto;

    public DetalleFacturas() {
    }

    public DetalleFacturas(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getMontoFinalProducto() {
        return montoFinalProducto;
    }

    public void setMontoFinalProducto(Double montoFinalProducto) {
        this.montoFinalProducto = montoFinalProducto;
    }

    public Integer getId_factura() {
        return id_factura;
    }

    public void setId_factura(Integer id_factura) {
        this.id_factura = id_factura;
    }

    public Integer getId_producto() {
        return id_producto;
    }

    public void setId_producto(Integer id_producto) {
        this.id_producto = id_producto;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalle != null ? idDetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleFacturas)) {
            return false;
        }
        DetalleFacturas other = (DetalleFacturas) object;
        if ((this.idDetalle == null && other.idDetalle != null) || (this.idDetalle != null && !this.idDetalle.equals(other.idDetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.DetalleFacturas[ idDetalle=" + idDetalle + " ]";
    }

}

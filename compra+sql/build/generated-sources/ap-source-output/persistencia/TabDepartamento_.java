package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabGerencia;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabDepartamento.class)
public class TabDepartamento_ { 

    public static volatile SingularAttribute<TabDepartamento, Integer> idDepartamento;
    public static volatile SingularAttribute<TabDepartamento, String> nombreDepartamento;
    public static volatile CollectionAttribute<TabDepartamento, TabGerencia> tabGerenciaCollection;

}
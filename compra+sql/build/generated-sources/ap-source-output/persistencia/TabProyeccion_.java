package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabDetalleVenta;
import persistencia.TabInventario;
import persistencia.TabPedido;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabProyeccion.class)
public class TabProyeccion_ { 

    public static volatile SingularAttribute<TabProyeccion, Integer> proyeccionmensual;
    public static volatile SingularAttribute<TabProyeccion, TabDetalleVenta> venta;
    public static volatile SingularAttribute<TabProyeccion, Integer> idProyeccion;
    public static volatile CollectionAttribute<TabProyeccion, TabPedido> tabPedidoCollection;
    public static volatile SingularAttribute<TabProyeccion, Integer> necesidadcompra;
    public static volatile SingularAttribute<TabProyeccion, TabInventario> inventario;

}
package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabEstadoPedido;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabEstado.class)
public class TabEstado_ { 

    public static volatile SingularAttribute<TabEstado, Integer> idEstado;
    public static volatile CollectionAttribute<TabEstado, TabEstadoPedido> tabEstadoPedidoCollection;
    public static volatile SingularAttribute<TabEstado, String> nombre;

}
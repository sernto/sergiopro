package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabEstado;
import persistencia.TabPedido;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabEstadoPedido.class)
public class TabEstadoPedido_ { 

    public static volatile SingularAttribute<TabEstadoPedido, Integer> idEstadoPedido;
    public static volatile SingularAttribute<TabEstadoPedido, TabPedido> codigoPedido;
    public static volatile SingularAttribute<TabEstadoPedido, TabEstado> estado;
    public static volatile SingularAttribute<TabEstadoPedido, String> fechaEstado;

}
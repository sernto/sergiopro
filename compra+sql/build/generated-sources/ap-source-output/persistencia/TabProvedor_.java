package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabGiro;
import persistencia.TabProducto;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabProvedor.class)
public class TabProvedor_ { 

    public static volatile SingularAttribute<TabProvedor, String> nombreProveedor;
    public static volatile CollectionAttribute<TabProvedor, TabProducto> tabProductoCollection;
    public static volatile SingularAttribute<TabProvedor, Integer> idProvedor;
    public static volatile SingularAttribute<TabProvedor, TabGiro> giros;
    public static volatile SingularAttribute<TabProvedor, String> telefono;

}
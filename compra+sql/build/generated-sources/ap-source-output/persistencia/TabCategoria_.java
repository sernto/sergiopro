package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabProducto;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabCategoria.class)
public class TabCategoria_ { 

    public static volatile CollectionAttribute<TabCategoria, TabProducto> tabProductoCollection;
    public static volatile SingularAttribute<TabCategoria, Integer> idCategoria;
    public static volatile SingularAttribute<TabCategoria, String> nombreCategoria;

}
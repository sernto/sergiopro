package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabDepartamento;
import persistencia.TabPedido;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabGerencia.class)
public class TabGerencia_ { 

    public static volatile SingularAttribute<TabGerencia, String> pass;
    public static volatile CollectionAttribute<TabGerencia, TabPedido> tabPedidoCollection;
    public static volatile SingularAttribute<TabGerencia, Integer> idGerencia;
    public static volatile SingularAttribute<TabGerencia, String> apellido;
    public static volatile SingularAttribute<TabGerencia, TabDepartamento> departamento;
    public static volatile SingularAttribute<TabGerencia, String> usuario;
    public static volatile SingularAttribute<TabGerencia, String> nombre;
    public static volatile SingularAttribute<TabGerencia, String> codEmpleado;

}
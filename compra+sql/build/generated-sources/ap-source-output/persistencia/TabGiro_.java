package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabProvedor;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabGiro.class)
public class TabGiro_ { 

    public static volatile CollectionAttribute<TabGiro, TabProvedor> tabProvedorCollection;
    public static volatile SingularAttribute<TabGiro, Integer> idGiro;
    public static volatile SingularAttribute<TabGiro, String> nombreGiro;

}
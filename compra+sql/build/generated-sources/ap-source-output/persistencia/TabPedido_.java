package persistencia;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabEstadoPedido;
import persistencia.TabGerencia;
import persistencia.TabProyeccion;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabPedido.class)
public class TabPedido_ { 

    public static volatile SingularAttribute<TabPedido, Integer> codigoPedido;
    public static volatile SingularAttribute<TabPedido, Double> total;
    public static volatile CollectionAttribute<TabPedido, TabEstadoPedido> tabEstadoPedidoCollection;
    public static volatile SingularAttribute<TabPedido, Double> iva;
    public static volatile SingularAttribute<TabPedido, TabProyeccion> proyeccion;
    public static volatile SingularAttribute<TabPedido, TabGerencia> gerencia;
    public static volatile SingularAttribute<TabPedido, Date> fechaPedido;

}
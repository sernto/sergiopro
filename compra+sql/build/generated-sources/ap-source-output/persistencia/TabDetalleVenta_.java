package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabProyeccion;
import persistencia.TabVenta;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabDetalleVenta.class)
public class TabDetalleVenta_ { 

    public static volatile SingularAttribute<TabDetalleVenta, String> fecha;
    public static volatile SingularAttribute<TabDetalleVenta, Integer> idDetalle;
    public static volatile SingularAttribute<TabDetalleVenta, String> comprador;
    public static volatile CollectionAttribute<TabDetalleVenta, TabProyeccion> tabProyeccionCollection;
    public static volatile SingularAttribute<TabDetalleVenta, TabVenta> idVenta;

}
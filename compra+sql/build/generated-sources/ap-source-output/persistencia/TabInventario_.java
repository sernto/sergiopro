package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabProducto;
import persistencia.TabProyeccion;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabInventario.class)
public class TabInventario_ { 

    public static volatile SingularAttribute<TabInventario, Integer> existencias;
    public static volatile SingularAttribute<TabInventario, Integer> entradas;
    public static volatile SingularAttribute<TabInventario, Integer> salidad;
    public static volatile SingularAttribute<TabInventario, TabProducto> producto;
    public static volatile SingularAttribute<TabInventario, Integer> stock;
    public static volatile SingularAttribute<TabInventario, Integer> idInventario;
    public static volatile CollectionAttribute<TabInventario, TabProyeccion> tabProyeccionCollection;

}
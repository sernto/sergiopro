package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabCategoria;
import persistencia.TabInventario;
import persistencia.TabProvedor;
import persistencia.TabVenta;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabProducto.class)
public class TabProducto_ { 

    public static volatile SingularAttribute<TabProducto, String> codigo;
    public static volatile SingularAttribute<TabProducto, Double> precio;
    public static volatile SingularAttribute<TabProducto, TabCategoria> categoria;
    public static volatile SingularAttribute<TabProducto, TabProvedor> provedor;
    public static volatile CollectionAttribute<TabProducto, TabVenta> tabVentaCollection;
    public static volatile SingularAttribute<TabProducto, String> nombre;
    public static volatile CollectionAttribute<TabProducto, TabInventario> tabInventarioCollection;

}
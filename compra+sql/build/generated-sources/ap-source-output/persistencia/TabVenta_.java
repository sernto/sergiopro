package persistencia;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import persistencia.TabDetalleVenta;
import persistencia.TabProducto;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-02-13T11:32:07")
@StaticMetamodel(TabVenta.class)
public class TabVenta_ { 

    public static volatile SingularAttribute<TabVenta, Double> total;
    public static volatile SingularAttribute<TabVenta, TabProducto> codigo;
    public static volatile CollectionAttribute<TabVenta, TabDetalleVenta> tabDetalleVentaCollection;
    public static volatile SingularAttribute<TabVenta, Integer> cantidad;
    public static volatile SingularAttribute<TabVenta, Integer> idVenta;

}
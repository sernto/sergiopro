-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema compras
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema compras
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `compras` DEFAULT CHARACTER SET latin1 ;
USE `compras` ;

-- -----------------------------------------------------
-- Table `compras`.`tab_categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_categoria` (
  `id_categoria` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_categoria`))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_departamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_departamento` (
  `id_departamento` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_departamento` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_departamento`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_giro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_giro` (
  `id_giro` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_giro` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_giro`))
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_estado_global`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_estado_global` (
  `id_estado_global` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_estado_global`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_provedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_provedor` (
  `id_provedor` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre_proveedor` VARCHAR(100) NULL DEFAULT NULL,
  `telefono` VARCHAR(100) NULL DEFAULT NULL,
  `giros` INT(11) NOT NULL,
  `estado_global` INT NOT NULL,
  PRIMARY KEY (`id_provedor`),
  INDEX `giros` (`giros` ASC),
  INDEX `estado_global_idx` (`estado_global` ASC),
  CONSTRAINT `giros`
    FOREIGN KEY (`giros`)
    REFERENCES `compras`.`tab_giro` (`id_giro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `estado_global`
    FOREIGN KEY (`estado_global`)
    REFERENCES `compras`.`tab_estado_global` (`id_estado_global`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_producto` (
  `codigo` VARCHAR(100) NOT NULL,
  `nombre` VARCHAR(100) NULL DEFAULT NULL,
  `categoria` INT(11) NOT NULL,
  `provedor` INT(11) NOT NULL,
  `precio` DOUBLE(7,2) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `categoria` (`categoria` ASC),
  INDEX `provedor` (`provedor` ASC),
  CONSTRAINT `tab_producto_ibfk_1`
    FOREIGN KEY (`categoria`)
    REFERENCES `compras`.`tab_categoria` (`id_categoria`),
  CONSTRAINT `tab_producto_ibfk_2`
    FOREIGN KEY (`provedor`)
    REFERENCES `compras`.`tab_provedor` (`id_provedor`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_venta` (
  `id_venta` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(100) NULL DEFAULT NULL,
  `cantidad` INT(11) NULL DEFAULT NULL,
  `total` DOUBLE(7,2) NULL DEFAULT NULL,
  PRIMARY KEY (`id_venta`),
  INDEX `codigo` (`codigo` ASC),
  CONSTRAINT `tab_venta_ibfk_1`
    FOREIGN KEY (`codigo`)
    REFERENCES `compras`.`tab_producto` (`codigo`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_detalle_venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_detalle_venta` (
  `id_detalle` INT(11) NOT NULL AUTO_INCREMENT,
  `id_venta` INT(11) NOT NULL,
  `fecha` VARCHAR(100) NULL DEFAULT NULL,
  `comprador` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_detalle`),
  INDEX `id_venta` (`id_venta` ASC),
  CONSTRAINT `tab_detalle_venta_ibfk_1`
    FOREIGN KEY (`id_venta`)
    REFERENCES `compras`.`tab_venta` (`id_venta`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_estado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_estado` (
  `id_estado` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_estado`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_inventario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_inventario` (
  `id_inventario` INT(11) NOT NULL AUTO_INCREMENT,
  `producto` VARCHAR(100) NOT NULL,
  `stock` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_inventario`),
  INDEX `producto` (`producto` ASC),
  CONSTRAINT `tab_inventario_ibfk_1`
    FOREIGN KEY (`producto`)
    REFERENCES `compras`.`tab_producto` (`codigo`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_proyeccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_proyeccion` (
  `id_proyeccion` INT(11) NOT NULL AUTO_INCREMENT,
  `inventario` INT(11) NOT NULL,
  `venta` INT(11) NOT NULL,
  `proyeccionmensual` INT(11) NULL DEFAULT NULL,
  `necesidadcompra` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_proyeccion`),
  INDEX `inventario` (`inventario` ASC),
  INDEX `venta` (`venta` ASC),
  CONSTRAINT `inventario`
    FOREIGN KEY (`inventario`)
    REFERENCES `compras`.`tab_inventario` (`id_inventario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `venta`
    FOREIGN KEY (`venta`)
    REFERENCES `compras`.`tab_detalle_venta` (`id_detalle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 42
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_gerencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_gerencia` (
  `id_gerencia` INT(11) NOT NULL AUTO_INCREMENT,
  `departamento` INT(11) NOT NULL,
  `nombre` VARCHAR(100) NULL DEFAULT NULL,
  `apellido` VARCHAR(100) NULL DEFAULT NULL,
  `cod_empleado` VARCHAR(100) NULL DEFAULT NULL,
  `usuario` VARCHAR(100) NULL DEFAULT NULL,
  `pass` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id_gerencia`),
  INDEX `departamento` (`departamento` ASC),
  CONSTRAINT `tab_gerencia_ibfk_1`
    FOREIGN KEY (`departamento`)
    REFERENCES `compras`.`tab_departamento` (`id_departamento`))
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_pedido` (
  `codigo_pedido` INT(11) NOT NULL AUTO_INCREMENT,
  `proyeccion` INT(11) NOT NULL,
  `gerencia` INT(11) NOT NULL,
  `fecha_pedido` DATE NOT NULL,
  `iva` DOUBLE(7,2) NULL DEFAULT NULL,
  `total` DOUBLE(7,2) NULL DEFAULT NULL,
  PRIMARY KEY (`codigo_pedido`),
  INDEX `proyeccion` (`proyeccion` ASC),
  INDEX `gerencia` (`gerencia` ASC),
  CONSTRAINT `tab_pedido_ibfk_1`
    FOREIGN KEY (`proyeccion`)
    REFERENCES `compras`.`tab_proyeccion` (`id_proyeccion`),
  CONSTRAINT `tab_pedido_ibfk_2`
    FOREIGN KEY (`gerencia`)
    REFERENCES `compras`.`tab_gerencia` (`id_gerencia`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `compras`.`tab_estado_pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`tab_estado_pedido` (
  `id_estado_pedido` INT(11) NOT NULL AUTO_INCREMENT,
  `codigo_pedido` INT(11) NOT NULL,
  `fecha_estado` VARCHAR(100) NULL DEFAULT NULL,
  `estado` INT(11) NOT NULL,
  PRIMARY KEY (`id_estado_pedido`),
  INDEX `codigo_pedido` (`codigo_pedido` ASC),
  INDEX `estado` (`estado` ASC),
  CONSTRAINT `tab_estado_pedido_ibfk_1`
    FOREIGN KEY (`codigo_pedido`)
    REFERENCES `compras`.`tab_pedido` (`codigo_pedido`),
  CONSTRAINT `tab_estado_pedido_ibfk_2`
    FOREIGN KEY (`estado`)
    REFERENCES `compras`.`tab_estado` (`id_estado`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

USE `compras` ;

-- -----------------------------------------------------
-- Placeholder table for view `compras`.`pedido_proyeccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `compras`.`pedido_proyeccion` (`id_inventario` INT, `codigo` INT, `nombre` INT, `stock` INT);

-- -----------------------------------------------------
-- procedure new_procedure
-- -----------------------------------------------------

DELIMITER $$
USE `compras`$$
CREATE DEFINER=`kz`@`%` PROCEDURE `new_procedure`(
in id_proyeccion int(11),
in inventario int(11),
in venta int(11),
in proyeccionmensual int(11)
)
BEGIN
insert into tab_proyeccion (id_proyeccion,inventario,venta,proyeccionmensual) values(id_proyeccion,inventario,venta,proyeccionmensual);
END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `compras`.`pedido_proyeccion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `compras`.`pedido_proyeccion`;
USE `compras`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`kz`@`%` SQL SECURITY DEFINER VIEW `compras`.`pedido_proyeccion` AS select `inventario`.`id_inventario` AS `id_inventario`,`produc`.`codigo` AS `codigo`,`produc`.`nombre` AS `nombre`,`inventario`.`stock` AS `stock` from (((`compras`.`tab_detalle_venta` `deta` join `compras`.`tab_venta` `vent` on((`deta`.`id_venta` = `vent`.`id_venta`))) join `compras`.`tab_producto` `produc` on((`vent`.`codigo` = `produc`.`codigo`))) join `compras`.`tab_inventario` `inventario` on((`produc`.`codigo` = `inventario`.`producto`)));

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
